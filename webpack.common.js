const path = require('path');
const webpack = require('webpack')

const settings = require('./webpack.settings.js');

const plugins = {
  progress: require('webpackbar'),
  html: require('html-webpack-plugin'),
  workbox: require('workbox-webpack-plugin'),
  manifest: require('webpack-pwa-manifest'),
  vue: require('vue-loader/lib/plugin')
}

const configureBabelLoader = () => {
  return {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
      loader: 'babel-loader',
      options: {
        presets: [
          '@babel/preset-env'
        ]
      }
    }
  }
}

const configureEntries = () => {
  let entries = {};
  for (const [key, value] of Object.entries(settings.entries)) {
    entries[key] = value;
  }
  return entries;
}

const configureFontLoader = () => {
  return {
    test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
    exclude: /images/,
    use: [{
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: 'fonts/',
        publicPath: '../fonts/' // use relative urls
      }
    }]
  }
}

const configureHandlebarsLoader = () => {
  return {
    test: /\.hbs$/,
    loader: 'handlebars-loader',
    options: {
      knownHelpersOnly: false,
      helperDirs: [],
      partialDirs: [path.resolve(__dirname, settings.paths.views + 'partials/')]
    }
  }
}

const configureHtml = (page) => {
  return {
    template: settings.paths.views + page + '.hbs',
    info: settings.info,
    minify: {
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    }
  }
}

const configureHtmlLoader = () => {
  return {
    test: /\.html$/,
    use: {
      loader: 'html-loader',
      options: {
        attrs: settings.htmlLoaderConfig.attrs,
        minimize: true,
        removeComments: true,
        collapseWhitespace: true,
        removeScriptTypeAttributes: true,
        removeStyleTypeAttributes: true
      }
    }
  }
}

const configureManifest = () => {
  return {
    name: settings.manifestConfig.name,
    short_name: settings.manifestConfig.short_name,
    background_color: settings.manifestConfig.background_color,
    display: settings.manifestConfig.display,
    theme_color: settings.manifestConfig.theme_color,
    start_url: settings.manifestConfig.start_url,
    name: settings.manifestConfig.name,
    icons: [
      {
        src: path.resolve(settings.paths.src.images + 'icon.jpg'),
        sizes: [24, 48, 64, 128, 192, 256, 384, 512],
        destination: path.join('images', 'icons')
      }
    ]
  }
}

const configureWorkbox = () => {
  return settings.workboxConfig
}

const configureVueLoader = () => {
  return {
    test: /\.vue$/,
    loader: 'vue-loader'
  }
}

module.exports = {
  entry: configureEntries(),
  module: {
    rules: [
      configureBabelLoader(),
      configureFontLoader(),
      configureHandlebarsLoader(),
      configureHtmlLoader(),
      configureVueLoader()
    ]
  },
  output: {
    path: path.resolve(__dirname, settings.paths.dist.base)
  },
  plugins: (() => {
    let temp = [
      new webpack.LoaderOptionsPlugin({
        options: { handlebarsLoader: {} }
      }),
      new plugins.progress({
        color: '#5C95EE'
      }),
      new plugins.workbox.GenerateSW(
        configureWorkbox()
      ),
      new plugins.manifest(
        configureManifest()
      ),
      new plugins.vue()
    ]
    // Por cada página agregamos un plugin
    settings.pages.forEach(page => {
      temp.push(
        new plugins.html(
          configureHtml(page)
        )
      )
    })
    return temp
  })(),

  stats: {
    children: false,
  },

  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    alias: {
      '~': path.resolve(__dirname, 'src/scripts/')
    }
  }
}
