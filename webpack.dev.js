const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge');
const sane = require('sane')

const settings = require('./webpack.settings.js');
const common = require('./webpack.common.js');

const configureDevServer = () => {
  return {
    contentBase: path.resolve(__dirname, settings.paths.src.base),
    port: 8080,
    host: 'localhost',
    quiet: true,
    overlay: {
      warnings: true,
      errors: true
    },
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    before: (app, server) => {
      const watcher = sane(path.resolve(__dirname, settings.paths.views), {
        glob: ['**/*'],
        poll: false,
      });
      watcher.on('change', function (filePath, root, stat) {
        server.sockWrite(server.sockets, 'content-changed');
      });
    }
  }
}

const configureImageLoader = () => {
  return {
    test: /\.(gif|png|jpe?g|svg)$/i,
    exclude: [
      '/fonts/'
    ],
    use: [
      {
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      }
    ]
  }
}

const configurePostcssLoader = () => {
  return {
    test: /\.((s[ac]|c)ss)$/,
    use: [
      {
          loader: 'style-loader'
      },
      {
        loader: 'css-loader',
        options: {
          sourceMap: true
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          sourceMap: true
        }
      },
      {
        loader: 'sass-loader',
        options: {
          outputStyle: 'expanded',
          sourceMap: true
        }
      }
    ]
  }
}

module.exports = () => {
  return merge(
    common, {
      output: {
        filename: '[name].js',
        publicPath: settings.devServerConfig.public() + '/',
      },

      devServer: configureDevServer(),

      devtool: 'source-map',

      module: {
        rules: [
          configurePostcssLoader(),
          configureImageLoader()
        ]
      }
    }
  )
}
