const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge');
const git = require('git-rev-sync');
const moment = require('moment');

const pkg = require('./package.json');
const settings = require('./webpack.settings.js');
const common = require('./webpack.common.js');

const plugins = {
  bundleAnalyzer: require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
  clean: require('clean-webpack-plugin'),
  extractCSS: require('mini-css-extract-plugin'),
  copy: require('copy-webpack-plugin'),
  sri: require('webpack-subresource-integrity')
}

const configureBanner = () => {
  let long = 'No git repository'
  let branch = 'empty'
  try {
    long = git.long()
    branch = git.branch()
  } catch (error) {}
  return {

    banner: [
      '/*!',
      ' * @project        ' + settings.name,
      ' * @name           ' + '[filebase]',
      ' * @author         ' + pkg.author.name,
      ' * @build          ' + moment().format('llll') + ' ET',
      ' * @release        ' + long + ' [' + branch + ']',
      ' * @copyright      Copyright (c) ' + moment().format('YYYY') + ' ' + settings.copyright,
      ' *',
      ' */',
      ''
    ].join('\n'),
    raw: true
  };
};

const configurePostcssLoader = () => {
  return {
    test: /\.((s[ac]|c)ss)$/,
    use: [
      {
        loader: plugins.extractCSS.loader,
        options: {
          // Corrige la ruta de css background en production
          publicPath: '../'
        }
      },
      {
        loader: 'css-loader',
        options: {
          sourceMap: false
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          sourceMap: false
        }
      },
      {
        loader: 'sass-loader',
        options: {
          outputStyle: 'expanded',
          sourceMap: false
        }
      }
    ]
  }
}

const configureImageLoader = () => {
  return {
    test: /\.(gif|png|jpe?g|svg)$/i,
    exclude: [
      '/fonts/'
    ],
    use: [
      {
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]?[hash:7]'
        }
      },
      {
        loader: 'image-webpack-loader',
        options: {
          bypassOnDebug: false,
          mozjpeg: {
            progressive: true,
            quality: 65
          },
          optipng: {
            enabled: false
          },
          pngquant: {
            quality: '65-90',
            speed: 4
          },
          gifsicle: {
            interlaced: false
          }
        }
      }
    ]
  }
}

const configureSri = () => {
  return {
    hashFuncNames: ['sha384'],
    enabled: true
  }
}

const configureCleanWebpack = () => [
  settings.paths.dist.base
]

const configureBundleAnalyzer = () => {
  return {
    analyzerMode: 'static',
    reportFilename: 'report.html',
    openAnalyzer: false
  }
}

module.exports = (env = {}, argv) => {
  return merge(
    common,
    {
      output: {
        filename: path.join(settings.paths.dist.scripts , '[name].js?[chunkhash]'),
        chunkFilename: path.join(settings.paths.dist.scripts, 'vendor.[id].js?[chunkhash]'),
        crossOriginLoading: 'anonymous'
      },

      module: {
        rules: [
          configurePostcssLoader(),
          configureImageLoader()
        ]
      },

      plugins: [
        new plugins.extractCSS({
          filename: 'styles/[name].css?[chunkhash]'
        }),
        new webpack.BannerPlugin(
          configureBanner()
        ),
        new plugins.bundleAnalyzer(
          configureBundleAnalyzer()
        ),
        new plugins.clean(
          configureCleanWebpack()
        ),
        new plugins.copy(
          settings.copyWebpackConfig
        ),
        new plugins.sri(
          configureSri()
        )
      ],

      devtool: '',

      optimization: {
        splitChunks: {
          chunks: 'all'
        }
      }
    }
  )
}
